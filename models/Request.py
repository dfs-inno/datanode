from dataclasses import dataclass
from typing import Any


@dataclass
class Request:
    message: Any
    app: Any
