import socketserver
from config import HOST, PORT
from TCPHandler import get_app
from routes import routes
import logging


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def main():
    app = get_app(routes, logger)
    socketserver.TCPServer.allow_reuse_address = True
    with socketserver.ThreadingTCPServer((HOST, PORT), app) as server:
        logger.info(f'Start server on {HOST}:{PORT}')
        try:
            server.serve_forever()
        except KeyboardInterrupt:
            print('Bye ...')


if __name__ == '__main__':
    main()




