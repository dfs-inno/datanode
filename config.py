from pathlib import Path
import os

NAMENODE_IP = os.environ.get("NAMENODE_HOST", "192.168.0.183")
NAMENODE_PORT = 5678

REDIS_HOST = os.environ.get("REDIS_HOST", "192.168.0.183")
REDIS_PORT = 6379

HOST, PORT = "0.0.0.0", 9600

CHUNK_SIZE = int(os.environ.get("CHUNK_SIZE", 1024))

NETWORK_INTERFACE = 'eth0'
MY_IP = os.environ.get("IP", None)

DATA_FOLDER = Path('./data/')
