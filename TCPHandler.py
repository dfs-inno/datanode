import json
import shutil
import socketserver
import random
import time
import netifaces as ni

from protos import client_to_nn_pb2, client_dn_to_dn_pb2, nn_to_client_pb2
from utils.RedisClient import RedisClient
from utils.decorators import logger_decorator
from utils.shortcuts import make_response, send_data, get_header, ack_file_save
from models.Request import Request
from threading import Thread
from config import DATA_FOLDER, NAMENODE_IP, NAMENODE_PORT, PORT, NETWORK_INTERFACE, MY_IP


def get_app(message_handlers, logger):
    if DATA_FOLDER.exists():
        shutil.rmtree(DATA_FOLDER)

    DATA_FOLDER.mkdir()

    redis = RedisClient(logger)

    redis.add_new_storage_server()
    redis.set_free_memory()

    heartbeat = Thread(target=send_heartbeat, args=(logger, redis.datanode_ip, ))
    heartbeat.start()

    logger.info("Replication status: IN PROGRESS")

    result = initial_replication(redis)

    logger.info(f"Replication status: {result}")

    class App(socketserver.BaseRequestHandler):

        def __init__(self, *args, **kwargs):
            self.redis = redis
            super(App, self).__init__(*args, **kwargs)

        def handle(self):
            length = self.request.recv(4)
            length = int.from_bytes(length, byteorder='big', signed=False)
            data = self.request.recv(length)

            message = client_dn_to_dn_pb2.DNCommand()
            message.ParseFromString(data)
            handler = logger_decorator(message_handlers.get(message.type, lambda x: None), logger)
            request = Request(message=message, app=self)
            response = handler(request)

            if type(response) is bytes:
                response_data = response
            elif response:
                response_data = response.SerializeToString()
            else:
                response_data = make_response("Incorrect message").SerializeToString()
            self.request.sendall(response_data)

    return App


def send_heartbeat(logger, ip):
    heartbeat = client_to_nn_pb2.HeartBeat()
    heartbeat.ip = ip

    command = client_to_nn_pb2.Command()
    command.type = client_to_nn_pb2.Command.COMMAND_TYPE.HEART_BEAT
    command.heart_beat.CopyFrom(heartbeat)
    msg = command.SerializeToString()
    header = get_header(msg)
    msg = header + msg

    while True:
        logger.info('[DN] -> <3 -> [NN]')
        result, _ = send_data(msg, NAMENODE_IP, NAMENODE_PORT)
        response = nn_to_client_pb2.Response()
        response.ParseFromString(result)
        logger.info(f"[DN] <- {response.status} <- [NN]")
        time.sleep(3)


def initial_replication(redis):
    command = client_to_nn_pb2.Command()
    command.type = client_to_nn_pb2.Command.COMMAND_TYPE.DUMP_FILE_SYSTEM

    msg = command.SerializeToString()
    header = get_header(msg)
    msg = header + msg

    message, _ = send_data(msg, NAMENODE_IP, NAMENODE_PORT)

    response = nn_to_client_pb2.Response()
    response.ParseFromString(message)

    data = json.loads(response.status)

    for inode in data['inodes']:
        for server in inode['servers']:
            try:
                get_file_from_datanode(inode['name'], server, inode['num_of_blocks'])
                break
            except ConnectionRefusedError:
                continue

    return "DONE"


def get_file_from_datanode(filename, datanode_ip, n_blocks):
    if MY_IP is None:
        ip = ni.ifaddresses(NETWORK_INTERFACE)[ni.AF_INET][0]['addr']
    else:
        ip = MY_IP

    for block_num in range(n_blocks):
        command = client_dn_to_dn_pb2.DNCommand()
        command.type = client_dn_to_dn_pb2.DNCommand.COMMAND_TYPE.READ

        action = client_dn_to_dn_pb2.ReadFile()
        action.file_name = filename
        action.chunk_number = block_num
        command.read_file.CopyFrom(action)

        msg = command.SerializeToString()
        header = get_header(msg)
        msg = header + msg
        _, sock = send_data(msg, datanode_ip, PORT, False)

        length = sock.recv(4)
        length = int.from_bytes(length, byteorder='big', signed=False)

        block = sock.recv(length)

        (DATA_FOLDER / f"{filename}_{block_num}").parent.mkdir(parents=True, exist_ok=True)
        with open(DATA_FOLDER / f"{filename}_{block_num}", 'wb') as f:
            f.write(block)

    ack_file_save(filename, ip)
