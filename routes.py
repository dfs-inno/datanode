from protos import client_dn_to_dn_pb2
from src import write_file_handler, get_file_handler


routes = {
    client_dn_to_dn_pb2.DNCommand.COMMAND_TYPE.WRITE: write_file_handler,
    client_dn_to_dn_pb2.DNCommand.COMMAND_TYPE.READ: get_file_handler
}
