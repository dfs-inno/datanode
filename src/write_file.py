from math import ceil
from multiprocessing import Process
from config import CHUNK_SIZE, DATA_FOLDER, NAMENODE_IP, NAMENODE_PORT
from protos import client_dn_to_dn_pb2, dn_to_client_pb2, client_to_nn_pb2
from utils.shortcuts import make_response, send_data, get_header,  ack_file_save


def write_file_handler(request):
    file_name = request.message.write_file.file_name
    replica_number = request.message.write_file.replica_number
    size = request.message.write_file.size

    make_response("ok")
    data = b''
    while len(data) < size:
        data += request.app.request.recv(size)

    num_chunks = ceil(size/CHUNK_SIZE)

    for chunk_number in range(num_chunks):
        f = open(DATA_FOLDER / f'{file_name}_{chunk_number}', 'wb')
        f.write(data[chunk_number*CHUNK_SIZE: (chunk_number + 1)*CHUNK_SIZE])
        f.close()

    request.app.redis.set_free_memory()

    ack_file_save(file_name, request.app.redis.datanode_ip)

    if replica_number == -1:
        p = Process(target=replicate, args=(request, data, file_name, size))
        p.start()

    return make_response('ok')


def replicate(request, data, filename, size):
    storage_servers = request.app.redis.get_all_storage_servers()

    if storage_servers:
        for storage_server in storage_servers:
            if storage_server != request.app.redis.datanode_ip:

                command = client_dn_to_dn_pb2.DNCommand()
                command.type = client_dn_to_dn_pb2.DNCommand.COMMAND_TYPE.WRITE
                action = client_dn_to_dn_pb2.WriteFile()

                action.file_name = filename
                action.replica_number = 0
                action.size = size
                command.write_file.CopyFrom(action)

                msg_bytes = command.SerializeToString()
                msg = get_header(msg_bytes) + msg_bytes + data
                received, _ = send_data(msg, ip=storage_server, port=9600)
                response = dn_to_client_pb2.DNResponse()
                response.ParseFromString(received)
