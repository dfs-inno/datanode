from config import DATA_FOLDER


def get_file_handler(request):
    file_name = request.message.read_file.file_name
    chunk_number = request.message.read_file.chunk_number

    with open(DATA_FOLDER / f'{file_name}_{chunk_number}', 'rb') as f:
        data = f.read()
        return len(data).to_bytes(4, byteorder='big', signed=False) + data
