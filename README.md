# Datanode

## How to run

### Install dependencies
```bash
pip install -r requirements.txt
pip install protobuf
```

### Environment variables
```bash
CHUNK_SIZE=1024 # Size of chunk, default value is 1024
REDIS_HOST=127.0.0.1 # Where is redis running
NAMENODE_HOST=127.0.0.1 # Where is namenode runnig
```

### Run without docker
```bash
export CHUNK_SIZE=1024
export REDIS_HOST=127.0.0.1
export NAMENODE_HOST=127.0.0.1
python datanode.py
```

### Run in docker container
```bash
docker run -p 9600:9600 -e CHUNK_SIZE=1024 -e REDIS_HOST=127.0.0.1 -e NAMENODE_HOST=127.0.0.1 wselfjes/datanode
```

### Run in docker swarm
```bash
docker service create \
-e REDIS_HOST=172.31.21.186 \
-e NAMENODE_HOST=172.31.21.186 \
-e CHUNK_SIZE=1048576 \
--publish published=9600,target=9600,mode=host\ 
--replicas 3 \
--name datanode wselfjes/datanode
```
