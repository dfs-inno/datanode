FROM python:3.7-alpine

RUN apk add --no-cache build-base libffi-dev python-dev linux-headers protobuf bind-tools
WORKDIR /app
COPY requirements.txt /app
RUN pip install -r requirements.txt
RUN pip install --upgrade protobuf
COPY . /app
RUN protoc -I=./protos/proto_files --python_out=./protos/ ./protos/proto_files/*.proto

CMD ["sh", "run.sh"]