from config import NAMENODE_IP, NAMENODE_PORT
from protos import dn_to_client_pb2, client_to_nn_pb2
import socket


def make_response(message: str) -> dn_to_client_pb2.DNResponse:
    message_response = dn_to_client_pb2.DNResponse()
    message_response.status = message
    return message_response


def get_header(data):
    header = len(data).to_bytes(4, byteorder='big', signed=False)
    return header


def send_data(data, ip, port, resp = True):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((ip, port))
    sock.sendall(data)
    if resp:
        response = get_msg(sock)
    else:
        response = ""

    return response, sock


def get_msg(sock, size=1024):
    response = b''
    while True:
        data = sock.recv(size)
        if data:
            response += data
        else:
            break

    return response


def ack_file_save(filename, datanode_ip):
    command = client_to_nn_pb2.Command()
    command.type = client_to_nn_pb2.Command.COMMAND_TYPE.UPDATE_LIST_SERVERS

    action = client_to_nn_pb2.UpdateFileIPs()
    action.filename = filename
    action.ip = datanode_ip
    command.update_file_ips.CopyFrom(action)

    msg_bytes = command.SerializeToString()
    msg = get_header(msg_bytes) + msg_bytes
    received, _ = send_data(msg, ip=NAMENODE_IP, port=NAMENODE_PORT)
    response = dn_to_client_pb2.DNResponse()
    response.ParseFromString(received)