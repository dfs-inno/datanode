import json
import shutil

import netifaces as ni
import redis
from config import REDIS_HOST, REDIS_PORT, DATA_FOLDER, NETWORK_INTERFACE, MY_IP
from utils.decorators import logger_decorator


class RedisClient:

    def __init__(self, logger, redis_host=REDIS_HOST, redis_port=REDIS_PORT):
        if MY_IP is None:
            self.datanode_ip = ni.ifaddresses(NETWORK_INTERFACE)[ni.AF_INET][0]['addr']
        else:
            self.datanode_ip = MY_IP

        self.__redis = redis.Redis(redis_host, redis_port)
        self.p = self.__redis.pubsub()
        self.p.subscribe(**{'rm-channel': logger_decorator(self.remove_file_handler, logger),
                            'mv-channel': logger_decorator(self.move_file_handler, logger),
                            'cp-channel': logger_decorator(self.copy_file_handler, logger),
                            'rmdir-channel': logger_decorator(self.remove_directory_handler, logger),
                            'mkdir-channel': logger_decorator(self.make_directory_handler, logger)})

        self.p.run_in_thread(sleep_time=0.1)

    def add_new_storage_server(self):
        return self.__redis.zadd("storage_ips", {self.datanode_ip: 0})

    def get_all_storage_servers(self):
        return [ip.decode('utf-8') for ip in self.__redis.zrange("storage_ips", 0, -1)]

    def set_free_memory(self):
        total, used, free = shutil.disk_usage("/")

        print(f"Free space available: {free // (2 ** 30)} GiB")

        total, used, free = shutil.disk_usage("/")

        return self.__redis.zadd("storage_ips", {self.datanode_ip: free}, xx = True)

    def remove_file_handler(self, message):
        file = message['data'].decode('utf-8')
        i = 0
        while True:
            if not (DATA_FOLDER / f'{file}_{i}').exists():
                break
            (DATA_FOLDER / f'{file}_{i}').unlink()
            i += 1

        self.set_free_memory()

    def move_file_handler(self, message):
        data = json.loads(message['data'].decode('utf-8'))
        file_src = data['from']
        file_dst = data['to']

        i = 0
        while True:
            if not (DATA_FOLDER / f'{file_src}_{i}').exists():
                break
            shutil.move(DATA_FOLDER / f'{file_src}_{i}', DATA_FOLDER / f'{file_dst}_{i}')
            i += 1

        self.set_free_memory()

    def copy_file_handler(self, message):
        data = json.loads(message['data'].decode('utf-8'))
        file_src = data['from']
        file_dst = data['to']

        i = 0
        while True:
            if not (DATA_FOLDER / f'{file_src}_{i}').exists():
                break
            shutil.copy2(DATA_FOLDER / f'{file_src}_{i}', DATA_FOLDER / f'{file_dst}_{i}')
            i += 1

        self.set_free_memory()

    def remove_directory_handler(self, message):
        directory = message['data'].decode('utf-8')

        if (DATA_FOLDER / directory).exists():
            if directory == ".":
                shutil.rmtree(DATA_FOLDER)
                DATA_FOLDER.mkdir()
            else:
                (DATA_FOLDER / directory).rmdir()

        self.set_free_memory()

    def make_directory_handler(self, message):
        directory = message['data'].decode('utf-8')

        if not (DATA_FOLDER / directory).exists():
            (DATA_FOLDER / directory).mkdir()

        self.set_free_memory()