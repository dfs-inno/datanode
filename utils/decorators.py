def logger_decorator(handler, logger):

    def wrapper(message):
        response = handler(message)
        logger.info(f"Input : {{ {str(message).strip()} }}, Output: {{ {str(response).strip()} }}")
        return response

    return wrapper
